# Données IGC pour QGis

Données de l'Inspection Générale des Carrières à utiliser dans le logiciel [QGis](https://www.qgis.org).

![Échantillon des données affichées dans QGis](screenshot.png)

## Définitions de couches

Les fichiers suivants définissent des couches. Il suffit de les glisser tels quels dans un projet :

- `fond_capgeo_igc.qlr` : Fond des cartes de l'atlas IGC (données en ligne [CapGEO](https://capgeo.sig.paris.fr/arcgis/rest/services/capgeo_igc/))
- `grille_numerotation.qlr` : Grille et numéro des cartes de l'atlas IGC (données en ligne [CapGEO](https://capgeo.sig.paris.fr/arcgis/rest/services/capgeo_igc/))
- `puits_atlas.qlr` : Puits annotés sur les cartes de l'atlas reportés manuellement (travail en cours)

## Puits de l'Atlas

Le fichier `puits_atlas.geojson` contient les données des puits reportés à la main depuis les cartes IGC. Il comporte les champs suivants :

- `type` : Type du puits tel que noté dans l'atlas (PS, Puits de service, ...) 
- `recouvsup` : Hauteur de recouvrement de l'étage supérieur ou principal.
- `hauteursup` : Hauteur de la galerie de l'étage supérieur ou principal.
- `recouvinf` : Hauteur de recouvrement de l'étage inférieur (si présent).
- `hauteurinf` : Hauteur de la galerie de l'étage inférieur (si présent).
- `cotesurface` : Cote de la surface quand elle est indiquée (rare).

## Méthode de travail

La liste des puits provient de la dernière édition des cartes qui est en ligne. Quelques problèmes peuvent avoir été rencontrés.

Certains puis ne sont pas formellement identifiables. Dans ce cas, si l'étoile (indiquant que la puits est annoté) est présente, c'est elle qui est pointé. Sinon, le puits est omis.

Quand la hauteur de galerie est une fouchette, c'est une valeur moyenne qui est relevée.

Les puits en coupe (ceux à deux étoiles) nécessite parfois des déductions pour trouver, par exemple, le recouvrement de la galerie inférieure, calculé en aditionnant le recouvrement de la galerie supérieure, sa hauteur, ainsi que l'épaisseur du banc séparant les deux étages.
